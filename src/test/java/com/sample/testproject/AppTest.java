package com.sample.testproject;

import org.testng.Assert;
import org.testng.annotations.Test;



/**
 * Unit test for simple App.
 */
public class AppTest 
{
	@Test
	public void positiveTest()
	{
		System.out.println("inside the test");
		String response=null;
		response=WebServiceMethods.GetMethod(null, "http://dixitd17-deepak-demo-autodevops-project.35.230.187.2.nip.io/" );
	System.out.println("the reponse is "+response);	
		
		Assert.assertTrue(response.equalsIgnoreCase("Hi Macys , GITLab AutoDevops pipeline is working great!!"));
	}
	
	@Test
	public void negativeTest()
	{
		System.out.println("inside the test");
		String response=null;
		response=WebServiceMethods.GetMethod(null, "http://dixitd17-deepak-demo-autodevops-project.35.230.187.2.nip.io/" );
	System.out.println("the reponse is "+response);	
		
		Assert.assertTrue(!response.equalsIgnoreCase("Hi Macys , GITLab AutoDevops pipeline is working great!!"));
	}
	
   
}
