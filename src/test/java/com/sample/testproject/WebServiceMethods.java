package com.sample.testproject;

import com.jayway.restassured.RestAssured;
import com.jayway.restassured.response.Response;
import com.jayway.restassured.specification.RequestSpecification;

public class WebServiceMethods {
	
	public static String PostMethod(String body,String url)
	{
		RestAssured.baseURI=url;
		RequestSpecification request=RestAssured.given();
		request.header("Content-Type","application/json");
		request.body(body);
		
		Response response=request.post();
		
		return response.asString();
		
		
	}
	
	public static String GetMethod(String body,String url)
	{
		RestAssured.baseURI=url;
		RequestSpecification request=RestAssured.given();
		request.header("Content-Type","application/json");
		if(body!=null)
		{
		request.body(body);
		}
		Response response=request.get();
		
		return response.asString();
		
		
	}

}
